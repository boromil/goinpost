package goinposthttp

import "net/http"

type RoundTripperOption func(rt *RoundTripper)

func RoundTripperWithBearer(bearer string) RoundTripperOption {
	return func(rt *RoundTripper) {
		rt.bearer = bearer
	}
}

type RoundTripper struct {
	baseRoundTripper http.RoundTripper

	bearer string
}

func NewRoundTripper(baseRoundTripper http.RoundTripper, options ...RoundTripperOption) *RoundTripper {
	rt := &RoundTripper{baseRoundTripper: baseRoundTripper}
	for _, option := range options {
		option(rt)
	}

	return rt
}

func (rt *RoundTripper) RoundTrip(req *http.Request) (*http.Response, error) {
	req.Header.Set("Authorization", "Bearer "+rt.bearer)

	return rt.baseRoundTripper.RoundTrip(req)
}
