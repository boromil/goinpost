package goinpost

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
)

const (
	DefaultBaseAPIAddreess = "https://api-shipx-pl.easypack24.net/v1"

	// trackingURLPart - /tracking/{tracking_number}
	trackingURLPart = "/tracking/"
	// statusesURLPart = "/statuses"
	statusesURLPart = "/statuses"
	// organizationDispatchPointsURLTemplate = /organizations/{organization_id}/dispatch_points
	organizationDispatchPointsURLTemplate = "/organizations/%s/dispatch_points"
	// organizationDispatchPointURLTemplate = /organizations/{organization_id}/dispatch_points/{id}
	organizationDispatchPointURLTemplate = organizationDispatchPointsURLTemplate + "/%s"
)

type Doer interface {
	Do(req *http.Request) (*http.Response, error)
}

type StatusesResp struct {
	Href  string   `json:"href"`
	Items []Status `json:"items"`
}

type DispatchPointsResp struct {
	Href    string          `json:"href"`
	Count   int             `json:"count"`
	PerPage int             `json:"per_page"`
	Page    int             `json:"page"`
	Items   []DispatchPoint `json:"items"`
}

type Client struct {
	baseAPIAdress string

	httpClient Doer
}

func NewClient(baseAPIAdress string, httpClient Doer) *Client {
	return &Client{
		baseAPIAdress: baseAPIAdress,
		httpClient:    httpClient,
	}
}

func (c *Client) Tracking(ctx context.Context, trackingNumber string) (*TrackingInfo, error) {
	if trackingNumber == "" {
		return nil, errors.New("no tracking number provided")
	}

	data, err := c.doRequest(
		ctx,
		http.MethodGet,
		c.baseAPIAdress+trackingURLPart+trackingNumber,
		nil,
	)
	if err != nil {
		return nil, fmt.Errorf("doRequest: %w", err)
	}

	var trackingInfo TrackingInfo
	if err := json.Unmarshal(data, &trackingInfo); err != nil {
		return nil, fmt.Errorf("json.Unmarshal: %w", err)
	}

	return &trackingInfo, nil
}

func (c *Client) Statuses(ctx context.Context) (*StatusesResp, error) {
	data, err := c.doRequest(
		ctx,
		http.MethodGet,
		c.baseAPIAdress+statusesURLPart,
		nil,
	)
	if err != nil {
		return nil, fmt.Errorf("doRequest: %w", err)
	}

	var statusesResp StatusesResp
	if err := json.Unmarshal(data, &statusesResp); err != nil {
		return nil, fmt.Errorf("json.Unmarshal: %w", err)
	}

	return &statusesResp, nil
}

func (c *Client) OrganizationDispatchPoints(
	ctx context.Context,
	organizationID string,
) (*DispatchPointsResp, error) {
	data, err := c.doRequest(
		ctx,
		http.MethodGet,
		c.baseAPIAdress+fmt.Sprintf(organizationDispatchPointsURLTemplate, organizationID),
		nil,
	)
	if err != nil {
		return nil, fmt.Errorf("doRequest: %w", err)
	}

	var dispatchPointsResp DispatchPointsResp
	if err := json.Unmarshal(data, &dispatchPointsResp); err != nil {
		return nil, fmt.Errorf("json.Unmarshal: %w", err)
	}

	return &dispatchPointsResp, nil
}

func (c *Client) OrganizationDispatchPoint(
	ctx context.Context,
	organizationID string,
	id string,
) (*DispatchPointsResp, error) {
	data, err := c.doRequest(
		ctx,
		http.MethodGet,
		c.baseAPIAdress+fmt.Sprintf(
			organizationDispatchPointURLTemplate,
			organizationID,
			id,
		),
		nil,
	)
	if err != nil {
		return nil, fmt.Errorf("doRequest: %w", err)
	}

	var dispatchPointsResp DispatchPointsResp
	if err := json.Unmarshal(data, &dispatchPointsResp); err != nil {
		return nil, fmt.Errorf("json.Unmarshal: %w", err)
	}

	return &dispatchPointsResp, nil
}

func (c *Client) doRequest(
	ctx context.Context,
	method string,
	requestURL string,
	body io.Reader,
) ([]byte, error) {
	req, err := http.NewRequestWithContext(ctx, method, requestURL, body)
	if err != nil {
		return nil, fmt.Errorf("http.NewRequestWithContext: %w", err)
	}

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("httpClient.Do: %w", err)
	}
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("ioutil.ReadAll: %w", err)
	}

	// the inpost api returns errors as data - no resp status code change
	var apiError APIError
	// ignore the unmarshall error as this can happen for non error data
	_ = json.Unmarshal(data, &apiError)
	if apiError.APIError != "" {
		return nil, apiError
	}

	return data, nil
}
