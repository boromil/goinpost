package goinpost

import (
	"time"
)

type DispatchPoint struct {
	Href        string                `json:"href"`
	ID          int                   `json:"id"`
	Name        string                `json:"name"`
	OfficeHours string                `json:"office_hours"`
	Phone       string                `json:"phone"`
	Email       *string               `json:"email"`
	Comments    *string               `json:"comments"`
	Status      string                `json:"status"`
	Address     *DispatchPointAddress `json:"address"`
}

type DispatchPointAddress struct {
	ID             int    `json:"id"`
	Street         string `json:"street"`
	BuildingNumber string `json:"building_number"`
	City           string `json:"city"`
	PostCode       string `json:"post_code"`
	CountryCode    string `json:"country_code"`
}

type Status struct {
	Name        string `json:"name"`
	Title       string `json:"title"`
	Description string `json:"description"`
}

type TrackingInfo struct {
	TrackingNumber   string           `json:"tracking_number"`
	Service          string           `json:"service"`
	Type             string           `json:"type"`
	Status           string           `json:"status"`
	CustomAttributes CustomAttributes `json:"custom_attributes"`
	TrackingDetails  []TrackingDetail `json:"tracking_details"`
	ExpectedFlow     []interface{}    `json:"expected_flow"`
	CreatedAt        time.Time        `json:"created_at"`
	UpdatedAt        time.Time        `json:"updated_at"`
}

type Location struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

type Address struct {
	Line1 string `json:"line1"`
	Line2 string `json:"line2"`
}

type TargetMachineDetail struct {
	Name                string   `json:"name"`
	OpeningHours        string   `json:"opening_hours"`
	LocationDescription string   `json:"location_description"`
	Location            Location `json:"location"`
	Address             Address  `json:"address"`
	Type                []string `json:"type"`
	Location247         bool     `json:"location247"`
}

type DropoffMachineDetail struct {
	Name                string   `json:"name"`
	OpeningHours        string   `json:"opening_hours"`
	LocationDescription string   `json:"location_description"`
	Location            Location `json:"location"`
	Address             Address  `json:"address"`
	Type                []string `json:"type"`
	Location247         bool     `json:"location247"`
}

type CustomAttributes struct {
	Size                 string               `json:"size"`
	TargetMachineID      string               `json:"target_machine_id"`
	DropoffMachineID     string               `json:"dropoff_machine_id"`
	TargetMachineDetail  TargetMachineDetail  `json:"target_machine_detail"`
	DropoffMachineDetail DropoffMachineDetail `json:"dropoff_machine_detail"`
	EndOfWeekCollection  bool                 `json:"end_of_week_collection"`
}

type TrackingDetail struct {
	Status       string    `json:"status"`
	OriginStatus string    `json:"origin_status"`
	Agency       *string   `json:"agency"`
	Datetime     time.Time `json:"datetime"`
}
