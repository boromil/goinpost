package goinpost

import (
	"encoding/json"
	"fmt"
)

type APIError struct {
	Status   int             `json:"status"`
	APIError string          `json:"error"`
	Message  string          `json:"message"`
	Details  json.RawMessage `json:"details,omitempty"`
}

func (e APIError) Error() string {
	return e.String()
}

func (e APIError) String() string {
	return fmt.Sprintf(
		`status": %d, error: %q, message: %q, details: %v`,
		e.Status,
		e.APIError,
		e.Message,
		string(e.Details),
	)
}
