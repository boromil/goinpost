package main

import (
	"context"
	"crypto/tls"
	"errors"
	"log"
	"net"
	"net/http"
	"time"

	"gitlab.com/boromil/goinpost"
	goinposthttp "gitlab.com/boromil/goinpost/http"
)

const (
	bearerToken = "somerandomtoken"
)

func main() {
	appCtx, cancelAppCtx := context.WithCancel(context.Background())
	defer cancelAppCtx()

	defaultTimeout := 30 * time.Second
	roundTripper := &http.Transport{
		Proxy: http.ProxyFromEnvironment,
		DialContext: (&net.Dialer{
			Timeout:   defaultTimeout,
			KeepAlive: defaultTimeout,
			DualStack: true,
		}).DialContext,
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
		ForceAttemptHTTP2:     true,
		MaxIdleConns:          100,
		IdleConnTimeout:       3 * defaultTimeout,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
	}
	roundTripperWsithBearer := goinposthttp.NewRoundTripper(
		roundTripper,
		goinposthttp.RoundTripperWithBearer(bearerToken),
	)
	httpClient := &http.Client{
		Transport: roundTripperWsithBearer,
		Timeout:   defaultTimeout,
	}

	inpostClient := goinpost.NewClient(
		goinpost.DefaultBaseAPIAddreess,
		httpClient,
	)
	statusesResponse, err := inpostClient.Statuses(appCtx)
	if err != nil {
		log.Fatalf("error getting statuses: %v", err)
	}
	log.Println("got some statuses, count:", len(statusesResponse.Items))

	_, err = inpostClient.Tracking(appCtx, "faketrackingnumer")
	checkForAPIError(err, http.StatusNotFound)

	_, err = inpostClient.OrganizationDispatchPoints(appCtx, "fakeorganationid")
	checkForAPIError(err, http.StatusUnauthorized)
}

func checkForAPIError(err error, expectedStatusCode int) {
	if err == nil {
		log.Fatalf("should have gotten an error")
	}

	var apiError goinpost.APIError
	if !errors.As(err, &apiError) {
		log.Fatalln("error should be of type goinpost.APIError")
	}
	if apiError.Status != expectedStatusCode {
		log.Fatalf(
			"should have gotten status code %d, got: %d",
			expectedStatusCode,
			apiError.Status,
		)
	}
	log.Printf("got expected error: %v", apiError)
}
